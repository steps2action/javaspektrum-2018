package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping("/")
    public String helloWorld (){
        return "Hello World!";
    }

    @RequestMapping("/produceError")
    public String throwException () throws Exception {
        throw new Exception ("");
    }
}
